package com.cedarhub.outlet;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

public class MainActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    //This is our tablayout
    private TabLayout tabLayout;

    //This is our viewPager
    private ViewPager viewPager;

      protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
          //setContentView(R.layout.signin);

          //Adding toolbar to the activity
          Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
          setSupportActionBar(toolbar);

          //Initializing the tablayout
          tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        //Adding the tabs using addTab() method
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.home));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.profile_icon));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.notification_icon));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.search_small));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

          //Initializing viewPager
          viewPager = (ViewPager) findViewById(R.id.pager);

          //Creating our pager adapter
          Pager adapter = new Pager(getSupportFragmentManager(), tabLayout.getTabCount());

          //Adding adapter to pager
          viewPager.setAdapter(adapter);

          //Adding onTabSelectedListener to swipe views
          tabLayout.setOnTabSelectedListener(this);

          // add on page listener for swipping the tab with the Title.
          viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

          //Adding adapter to pager
          viewPager.setAdapter(adapter);
          viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

//Adding onTabSelectedListener to swipe views
          tabLayout.setOnTabSelectedListener(this);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }


}
