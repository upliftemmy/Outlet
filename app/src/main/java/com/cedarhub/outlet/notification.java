package com.cedarhub.outlet;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by emmanuel on 25-Oct-16.
 */

public class notification extends Fragment {
    //overridden method on onCreateView
    @Override
    public  View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        //Returning the Layout file after inflating
        return inflater.inflate(R.layout.notification, container, false);
    }
}
