package com.cedarhub.outlet;

/**
 * Created by emmanuel on 25-Oct-16.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class profile extends Fragment {
    //overridden method on onCreateView
    @Override
    public  View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        //Returning the Layout file after inflating
        return inflater.inflate(R.layout.profile, container, false);
    }
}
